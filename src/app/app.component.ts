import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Tour of Heroes';
  environment = environment.environment;

  ngOnInit() {
    console.log('environment.environment' + environment.environment);
    console.log('environment' + environment);
  }
}
